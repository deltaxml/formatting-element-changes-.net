# Formatting Element Changes (.NET)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/formatting-element-changes`.*

---

Why inline formatting elements need special processing in order to minimise the impact that changing them has on the result file.

This document describes how to run the sample. For concept details see: [Formatting Element Changes](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/formatting-element-changes)

## Running the Sample
We provide a Visual Studio solution (.sln) file for the C# sample in the *dotnet-api* directory, and a Visual Studio project (.csproj) file may be found within the sample directory *FormattingElementDemo*.
Alternatively, the sample can be built and run without Visual Studio by running the *rundemo.bat* batch file - either from the command-line, or by double-clicking on it in the Windows File Explorer.

# **Note - .NET support has been deprecated as of version 10.0.0 **