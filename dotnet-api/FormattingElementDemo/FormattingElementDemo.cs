// Copyright (c) 2014 DeltaXML Ltd. All rights reserved

using DeltaXML.CoreS9Api;
using System;
using System.IO;

namespace DeltaXML.Demos
{
    public class FormattingElementDemo
    {
        public static void Main()
        {
            Console.WriteLine("Formatting Element Changes Sample");
            FileInfo input1 = new FileInfo("input1.xml");
            FileInfo input2 = new FileInfo("input2.xml");
            FileInfo basicResult = new FileInfo("no-format-result.xml");
            FileInfo formatAwareResult = new FileInfo("format-result.xml");
            FileInfo formatMarkerScript = new FileInfo("mark-formatting.xsl");

            Console.WriteLine("Initializing DocumentComparator");
            DocumentComparator comparator = new DocumentComparator();

            Console.WriteLine("Performing the basic comparison. Result is in " + basicResult.Name);
            comparator.compare(input1, input2, basicResult);

            FilterStepHelper fsh = comparator.newFilterStepHelper();
            FilterChain formatMarker = fsh.newSingleStepFilterChain(formatMarkerScript, "format-marker");

            comparator.setExtensionPoint(DocumentComparator.ExtensionPoint.PRE_FLATTENING, formatMarker);

            Console.WriteLine("Performing the format aware comparison. Result is in " + formatAwareResult.Name);
            comparator.compare(input1, input2, formatAwareResult);
        }
    }
}

